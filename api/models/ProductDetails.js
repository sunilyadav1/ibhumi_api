/**
 * ProductDetails.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

   variety:{type:'string'},

   quantity:{type:'string'},

   price:{type:'string'},

   location : { type : 'json'},

   fromtime : { type : 'string'},

   totime : { type : 'string'},

   home_delivery:{type:'string'},

   delivery_location:{type:'string'},

   age:{type:'string'},

   color:{type:'string'},

   pic:{type: 'json', columnType: 'array'},

   category_id:{model:'category'},

   product_id:{model:'products'},

   user_id:{model:'user'},

    

  },

};

