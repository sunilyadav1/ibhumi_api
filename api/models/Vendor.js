/**
 * Vendor.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
   
   shop_name:{type:"string"},

   name:{type:"string"},

   phone:{type:"string"},

   address:{type:"string"},

   location : { type : 'json'},

   products: {
      collection: 'Vendorproducts',
      via: 'vendor',
      
    },

   
  },

};

