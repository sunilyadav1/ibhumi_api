/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    name:{type:'string'},

    phone:{type:'string'},

    address:{type:'string'},

    email:{type:'String'},

    dob:{type:'string'},

    pincode:{type:'string'},

    pic:{type:'string'},

    did:{type:'string'},

    products: {
      collection:'productdetails',
      via: 'user_id'
    },


   

  },




};

