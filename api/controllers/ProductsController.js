/**
 * ProductsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
  add:function(req,res){

  	Products.create(req.body).fetch().exec(function(err,productData){
        
  		if(!err && productData){

  			res.json(productData)

  		}else{

  			res.json({err,"status":"false","message":"Unable to create category"})
  		}

  	})

  },


  search_products :function(req,res){

    Products.find({ name: { 'contains': req.param('name')} })
        .populate('products')
        .exec(function(err, products) {
     
      if (err) {
       
        return res.json(err);
      } else {

        var a = products[0].products;
       
        resData = {};
        resArr = [];

        asyncLoop(0, a.length, function(loop) {
         //console.log(a[loop.iteration()].pic[0])
          Products.find({id:a[loop.iteration()].product_id}).exec(function(err,productData){
            
              resArr.push({ 

                "product_name": productData[0].name, 
                "variety": a[loop.iteration()].variety, 
                "quantity": a[loop.iteration()].quantity, 
                "price": a[loop.iteration()].price, 
                "fromtime": a[loop.iteration()].fromtime, 
                "totime": a[loop.iteration()].totime, 
                "home_delivery":a[loop.iteration()].home_delivery,
                "delivery_location":a[loop.iteration()].delivery_location,
                "age":a[loop.iteration()].age,
                "color":a[loop.iteration()].color,
                "user_name": products[0].name, 
                "phone": products[0].phone, 
                "images":(a[loop.iteration()].pic) ? a[loop.iteration()].pic:"" , 
                
              });
             

              loop.next();
            })
        }, function() {
        
          resData = {
            "status": true,
            "message": "",
            "results": resArr
          }
          res.json(resData);
        })

      }

    })

 

  },

  getproducts_by_id:function(req,res){

    Products.find({id: req.body.product_id })
        .populate('products')
        .exec(function(err, products) {

      if (err) {
       
        return res.json(err);

      } else {
         
          resData = {};
          resArr = [];
          
          User.find({id: products[0].products[0].user_id }).exec(function(err, userdata) {
            // console.log(userdata)
            if(!err){

              resArr.push({ 

                    "product_name": products[0].name, 
                    "variety": products[0].products[0].variety, 
                    "quantity": products[0].products[0].quantity, 
                    "price": products[0].products[0].price, 
                    "fromtime": products[0].products[0].fromtime, 
                    "totime":  products[0].products[0].totime, 
                    "home_delivery":products[0].products[0].home_delivery,
                    "delivery_location":products[0].products[0].delivery_location,
                    "age": products[0].products[0].age,
                    "color": products[0].products[0].color,
                    "user_name":userdata[0].name,
                    "phone": userdata[0].phone, 
                    "images":(products[0].products[0].pic) ? req.protocol + '://' + req.host+':'+ req.port+'/images/' + products[0].products[0].pic:"" , 
                    
              });

          
              resData = {
                  "status": true,
                  "message": "",
                  "results": resArr
                }
                res.json(resData);

            }else{

            res.json(err);

           }
          

          
          });


        }

    })

  },

  //deletepost:function(req,res){

    // Products.destroy({ id: req.param('id') }).exec(function(err) {
    //   if (err) {
    //     return res.json({ err, "status": "false" });
    //   }else{
    //     ProductDetails.destroy({product_id: req.param('id') }).exec(function(err) {
    //       if(err){
    //           return res.json({ err, "status": "false" });
    //        }else{

    //           return res.ok({ "status": "true", "message": "post is deleted sccessfully" });
    //        }
    //     })
    //     return res.ok({ "status": "true", "message": "post is deleted sccessfully" });

    //   }

      
    // });

    //  ProductDetails.destroy({product_id: req.param('id') }).exec(function(err) {
    //   if (err) {
    //     return res.json({ err, "status": "false" });
    //   }else{
    //     Products.destroy({id: req.param('id') }).exec(function(err) {
    //       if(err){
    //           return res.json({ err, "status": "false" });
    //        }else{

    //           return res.json({ "status": "true", "message": "post is deleted sccessfully" });
    //        }
    //     })
    //     return res.json({ "status": "true", "message": "post is deleted sccessfully" });

    //   }

      
    // });

  //}





  // getproducts:function(req,res){

  // 	Products.find().exec(function(err,productData){

  // 		if(!err && productData){

  // 			res.json({result:productData,"status":"true","message":"Product Details"})
  // 		}else{

  // 			res.json({err,status:"false",message:"Unable to fetch products"})
  // 		}

  // 	})
  // }

};


function asyncLoop(startIndex,iterations, func, callback) {
  var index = startIndex;
  var done = false;
  var loop = {
      next: function() {
          if (done) {
              return;
          }

          if (index < iterations) {
              index++;
              func(loop);

          } else {
              done = true;
              callback();
          }
      },

      iteration: function() {
          return index - 1;
      },

      break: function() {
          done = true;
          callback();
      }
  };
  loop.next();
  return loop;
}

