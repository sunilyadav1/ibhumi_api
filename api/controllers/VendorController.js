/**
 * VendorController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {


  add:function(req,res){
    
    var products=JSON.parse(req.param('products'));
    var location=JSON.parse(req.param('location'));
    Vendor.create({shop_name:req.body.shop_name,name:req.body.name,phone:req.body.phone,address:req.body.address,
    location:location,products:products}).fetch().exec(function(err,result){
    	if(err)
    	{
    		res.json(err)
    	}else{
    		res.json(result)
    	}
    });



  },


search_vendor: function (req, res) {
var location=JSON.parse(req.param('location'));
   
    Vendor.native(function(err, collection) {
      if(!err){
        collection.find({location: {$near: location , $maxDistance : (4/111.12) }}).toArray(function (err, results) {
              if (!err){
                  res.json(results);
              } else {
                  res.json(err);
              }
          });
      }else {
        res.json(err);
      }
    });

}






	
  

}