/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

 var fs = require('fs');

module.exports = {


    signup: function(req, res) {
    
      User.findOne({phone: req.body.phone}).exec(function(err,resultData){

        if(!err && resultData)
        {
            let resData = {};
            resData = {
            "phone": (resultData.phone) ? resultData.phone : "",
            "id": resultData.id
            }
            return res.json({result: resData, "status": "true", "message": "Login successfully" });
        }
        else if(resultData==undefined)
        {
         
            User.create({phone: req.body.phone}).fetch().exec(function(err,userData){
             
              if(!err && userData)
              {
                let resData = {};
                resData = {
                "phone": (userData.phone) ? userData.phone : "",
                "id": userData.id
                }
                return res.json({result: resData, "status": "true", "message": "User created successfully" });
              }else
              {
                
                res.json(err);
              }

           })
        }
        else
        {
           res.json(err);
        }

      })
      
      
    },




    updateprofile: function(req, res) {

      var img = req.body.pic;
      
      var ext = 'jpeg';
     
      var data = img.replace(/^data:image\/\w+;base64,/, "");
     
      var buf = Buffer.from(data, 'base64');


      dirname = sails.config.appPath;
      
      var date = new Date();
      filename = date.getTime();

      imagepath = dirname + '/' + 'assets/images' + '/' + filename + '.' + ext;

      fs.writeFile(imagepath, buf, function(err) {

        if (err) {
          res.json(err);
        } else {
          var imgpath = imagepath.substr(imagepath.lastIndexOf("/"));
          var imageurl = imgpath.substr(1);
          User.update({ id: req.param('id') }, { pic: imageurl,name: req.body.name,phone: req.body.phone,address:req.body.address,email: req.body.email,
           dob: req.body.dob, pincode: req.body.pincode }).fetch().exec(function(err, userData) {
          
            if (!err && userData) {
              let resData = {};
              resData = {
                "name":(userData[0].name) ? userData[0].name:"",
                "phone":(userData[0].phone) ? userData[0].phone:"",
                "address":(userData[0].address) ? userData[0].address:"",
                "email":(userData[0].email) ? userData[0].email:"",
                "dob":(userData[0].dob) ? userData[0].dob:"",
                "pincode":(userData[0].pincode) ? userData[0].pincode:"",
                "pic": (userData[0].pic) ?  req.protocol + '://' + req.host+':'+ req.port+'/images/' + userData[0].pic : "",
                "status": "true",
                "message": "user details updated successfully"

              }

             
              res.json({result:resData});
            

            } else {
              res.json({err ,"status": "false", "message": "failed to update user details" });
            }

          });
        }
      });


    },



  profile:function(req,res){

    User.findOne({id:req.body.id}).exec(function(err,userData){


      if(!err && userData){
         
         res.json({result:userData,status:"true",message:"user details"})

      }else{

        res.json({err,status:"false",message:"unable to fetch user details"})
      }
    })
  },


  my_post: function(req, res){

    User.find({ id: req.body.user_id })
        .populate('products')
        .exec(function(err, products) {
     
      if (err) {
       
        return res.json(err);
      } else {

        var a = products[0].products;
       
        resData = {};
        resArr = [];

        asyncLoop(0, a.length, function(loop) {
         //console.log(a[loop.iteration()].pic[0])
          Products.find({id:a[loop.iteration()].product_id}).exec(function(err,productData){
            
              resArr.push({ 

                "product_name": productData[0].name, 
                "variety": a[loop.iteration()].variety, 
                "quantity": a[loop.iteration()].quantity, 
                "price": a[loop.iteration()].price, 
                "fromtime": a[loop.iteration()].fromtime, 
                "totime": a[loop.iteration()].totime, 
                "home_delivery":a[loop.iteration()].home_delivery,
                "delivery_location":a[loop.iteration()].delivery_location,
                "age":a[loop.iteration()].age,
                "color":a[loop.iteration()].color,
                "user_name": products[0].name, 
                "phone": products[0].phone, 
                "images":(a[loop.iteration()].pic) ? a[loop.iteration()].pic:"" , 
                
              });
             

              loop.next();
            })
        }, function() {
        
          resData = {
            "status": true,
            "message": "",
            "results": resArr
          }
          res.json(resData);
        })

      }

    })
  },


  

};

function asyncLoop(startIndex,iterations, func, callback) {
  var index = startIndex;
  var done = false;
  var loop = {
      next: function() {
          if (done) {
              return;
          }

          if (index < iterations) {
              index++;
              func(loop);

          } else {
              done = true;
              callback();
          }
      },

      iteration: function() {
          return index - 1;
      },

      break: function() {
          done = true;
          callback();
      }
  };
  loop.next();
  return loop;
}

