/**
 * ProductDetailsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
add : function(req, res) {

  var location=JSON.parse(req.param('location'));
    
  ProductDetails.create({variety:req.body.variety,quantity:req.body.quantity,price:req.body.price,location:location,
    fromtime:req.body.fromtime,totime:req.body.totime,home_delivery:req.body.home_delivery,
    delivery_location:req.body.delivery_location,age:req.body.age,color:req.body.color,category_id:req.body.category_id,product_id:req.body.product_id,user_id:req.body.user_id}).fetch().exec(function(err, producData) {
    
    if(!err && producData){
                
      dirname = sails.config.appPath;
      var collectedImages = [];
     
      req.file('images[]').upload({
      dirname: require('path').resolve(sails.config.appPath, 'assets/images')
      },function (err, uploadedFiles) {
                          
        if (err) {
          sails.log.debug("err adding images");
          return res.json(err);
        }else{
            
              if(uploadedFiles.length>0){

                  for(let i=0;i<uploadedFiles.length;i++){

                    var s = uploadedFiles[i].fd;
                    //collectedImages.push(uploadedFiles[i].fd);
                     var img = s.substr( s.lastIndexOf("/") ); 
                     collectedImages.push(img.substr(1));
                    //var imageurl = img.substr(1);
                    //collectedImages.push(s.substr( s.indexOf("/images") )  );
                   
                  }

                 
                  ProductDetails.update({id: producData.id }, { "pic" : collectedImages }).fetch().exec(function(err, resultData) {
                     if(!err){
                       return res.json(resultData[0]);
                     
                    }else{
                      return res.json(err);
                    }

                  });
              }else{

                    res.json(producData);
              }
                              
          } 

      });
           
          
    }else{
          
      res.json(err);

    }

  });

},

getproducts :function(req,res){

  ProductDetails.find().exec(function(err, products) {
     //res.json(products)
      if (err) {
       
        return res.json(err);
      } else {

        var a = products;
       
        resData = {};
        resArr = [];
      
        asyncLoop(0, a.length, function(loop) {
         //console.log(a[loop.iteration()].pic[0])
          Products.find({id:a[loop.iteration()].product_id}).exec(function(err,productData){
            
              resArr.push({ 
 
                "product_name": productData[0].name, 
                "variety": a[loop.iteration()].variety, 
                "quantity": a[loop.iteration()].quantity, 
                "price": a[loop.iteration()].price, 
                "fromtime": a[loop.iteration()].fromtime, 
                "totime": a[loop.iteration()].totime, 
                "home_delivery":a[loop.iteration()].home_delivery,
                "delivery_location":a[loop.iteration()].delivery_location,
                "age":a[loop.iteration()].age,
                "color":a[loop.iteration()].color,
                "user_name": products[0].name, 
                "phone": products[0].phone, 
                "images":(a[loop.iteration()].pic) ? req.protocol + '://' + req.host+':'+ req.port+'/images/' + a[loop.iteration()].pic:"" , 
                "product_id":(a[loop.iteration()].product_id) ?  a[loop.iteration()].product_id:"",
                "user_id":(a[loop.iteration()].user_id) ? a[loop.iteration()].user_id:""
                
              });
             

              loop.next();
            })
        }, function() {
        
          resData = {
            "status": true,
            "message": "",
            "results": resArr
          }
          res.json(resData);
        })

      }

    })

},





};

function asyncLoop(startIndex,iterations, func, callback) {
  var index = startIndex;
  var done = false;
  var loop = {
      next: function() {
          if (done) {
              return;
          }

          if (index < iterations) {
              index++;
              func(loop);

          } else {
              done = true;
              callback();
          }
      },

      iteration: function() {
          return index - 1;
      },

      break: function() {
          done = true;
          callback();
      }
  };
  loop.next();
  return loop;
}