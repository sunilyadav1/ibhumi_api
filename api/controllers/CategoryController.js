/**
 * CategoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    
  add:function(req,res){

  	Category.create(req.body).fetch().exec(function(err,categoryData){
        
  		if(!err && categoryData){

  			res.json(categoryData)

  		}else{

  			res.json({err,"status":"false","message":"Unable to create category"})
  		}

  	})

    },


    getcategories:function(req,res){

      Category.find().exec(function(err,categoryData){

        if(!err && categoryData){

          res.json({result:categoryData})
        }else{

          res.json({err,status:"false",message:"Unable to fetch categories"})
        }

      })
    },


    get_products_from_category: function(req, res){

      Category.find({id:req.body.category_id}).populate('products').exec(function(err, products){
            
          if(!err && products) {
          let results={};
          let resData=[];
          var a=products[0].products;
         

          for(let i=0;i<a.length;i++){
 
             resData.push({
              "product_name":a[i].name,
              "category_name":products[0].name,
              "product_id":a[i].id,
              "category_id":products[0].id,
            })
            
          }
          results={
            "status": "true",
            "message": "Product list",
            "products":resData
          }
         
         res.json(results)
          
        }else {
        res.negotiate({err, "status": "false","message": "unable to fetch Product details"});
        }
      });
    },



  // getcategories:function(req,res){

  // 	Category.find({ name: { 'contains': req.param('name') } }).exec(function(err,categoryData){

  // 		if(!err && categoryData){

  // 			res.json({result:categoryData})
  // 		}else{

  // 			res.json({err,status:"false",message:"Unable to fetch categories"})
  // 		}

  // 	})
  // }





};

