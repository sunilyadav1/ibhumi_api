/**
 * VendorproductsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {


	add:function(req,res){

  	Vendorproducts.create(req.body).fetch().exec(function(err,productData){
        
  		if(!err && productData){

  			res.json(productData)

  		}else{

  			res.json({err,"status":"false","message":"Unable to create category"})
  		}

  	})

  },



  // getproducts:function(req,res){

  // 	Products.find().exec(function(err,productData){

  // 		if(!err && productData){

  // 			res.json({result:productData,"status":"true","message":"Product Details"})
  // 		}else{

  // 			res.json({err,status:"false",message:"Unable to fetch products"})
  // 		}

  // 	})
  // }

  getvendorproducts:function(req,res){

    Vendorproducts.find().exec(function(err,vendorproductData){

      if(!err && vendorproductData){

        res.json({result:vendorproductData,"status":"true","message":"VendorProduct Details"})
      }else{

        res.json({err,status:"false",message:"Unable to fetch vendorproducts"})
      }

    })
  }
  

};

