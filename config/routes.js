/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },

   
  'POST /user/signup'                         : 'UserController.signup',
  'POST /user/updateprofile'                  : 'UserController.updateprofile',
  'POST /user/profile'                        : 'UserController.profile',
  'GET /category/getcategories'               : 'CategoryController.getcategories',
  'GET /products/getproducts'                 : 'ProductsController.getproducts',
  'POST /vendor/search_vendor'                : 'VendorController.search_vendor',
  'GET /vendor/getvendorproducts '            : 'VendorproductsController.getvendorproducts',
  'POST /category/get_products_from_category' : 'CategoryController.get_products_from_category',
  'POST /productdetails/add'                  : 'ProductDetailsController.add',
  'GET /productdetails/getproducts'           : 'ProductDetailsController.getproducts',
  'POST /products/search_products'            : 'ProductsController.search_products',
  'POST /products/getproducts_by_id'          : 'ProductsController.getproducts_by_id',
  //'POST /products/deletepost'                 : 'ProductsController.deletepost',
  'POST /user/my_post'                        : 'UserController.my_post',
  
  // 'POST /category/getcategories'           : 'CategoryController.getcategories',


  //Admin panel
  'POST /vendorproducts/add'                  : 'VendorproductsController.add',
  'POST /category/add'                        : 'CategoryController.add',
  'POST /products/add'                        : 'ProductsController.add',
  'POST /vendor/add'                          : 'VendorController.add',












  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
